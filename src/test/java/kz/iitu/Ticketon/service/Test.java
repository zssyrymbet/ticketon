package kz.iitu.Ticketon.service;
import kz.iitu.Ticketon.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Scanner;

@Component
public class Test {
    Scanner in = new Scanner(System.in);

    @Autowired
    private ConcertDao concertDao;

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private MusicalDao musicalDao;

    @Autowired
    private PlaceDao placeDao;

    @Autowired
    private SessionDao sessionDao;

    @Autowired
    private TicketDao ticketDao;

    public void createConcert(){
        System.out.println("Enter concert id: ");
        int id = in.nextInt();
        System.out.println("Enter concert name: ");
        String name = in.next();
        System.out.println("Enter concert description: ");
        String description = in.next();
        System.out.println("Enter concert location: ");
        String location = in.next();
        concertDao.createConcertQuery(id, name, description, location);
    }

    public void createMovie(){
        System.out.println("Enter movie id: ");
        int id = in.nextInt();
        System.out.println("Enter movie name: ");
        String name = in.next();
        System.out.println("Enter movie year: ");
        int year = in.nextInt();
        System.out.println("Enter movie country: ");
        String country = in.next();
        System.out.println("Enter movie description: ");
        String description = in.next();
        movieDao.createMovieQuery(id, name, year, country, description);
    }

    public void createMusical(){
        System.out.println("Enter musical id: ");
        int id = in.nextInt();
        System.out.println("Enter musical name: ");
        String name = in.next();
        System.out.println("Enter musical description: ");
        String description = in.next();
        musicalDao.createMusicalQuery(id, name, description);
    }

    public void createPlace(){
        System.out.println("Enter place id: ");
        int id = in.nextInt();
        System.out.println("Enter place name: ");
        String name = in.next();
        System.out.println("Enter place location: ");
        String location = in.next();
        System.out.println("Enter number of seats in place: ");
        int seat = in.nextInt();
        placeDao.createPlaceQuery(id, name, location, seat);
    }

    public void createSession(){
        System.out.println("Enter session id: ");
        int id = in.nextInt();
        System.out.println("Enter session time: ");
        String time = in.next();
        System.out.println("Concerts: ");
        concertDao.allFromConcerts();
        System.out.println("Movies: ");
        movieDao.allFromMovies();
        System.out.println("Musicals: ");
        musicalDao.allFromMusicals();
        System.out.println("Enter what do you want to book: Concert Movies or Musical");
        String answer = in.next();
        switch (answer) {
            case "Concert":
                System.out.println("Enter id of concert");
                int concert_id = in.nextInt();
                sessionDao.createSessionQuery(id, time,concert_id, 0, 0);
                break;
            case "Movie":
                System.out.println("Enter id of movie");
                int movie_id = in.nextInt();
                sessionDao.createSessionQuery(id, time,0, movie_id, 0);
                break;
            case "Musical":
                System.out.println("Enter id of musical");
                int musical_id = in.nextInt();
                sessionDao.createSessionQuery(id, time,0, 0, musical_id);
                break;
        }
    }

    public void createTicket(){
        System.out.println("Enter ticket id: ");
        int id = in.nextInt();
        System.out.println("Enter ticket price: ");
        double price = in.nextDouble();
        sessionDao.allFromSessions();
        System.out.println("Enter session id: ");
        int session_id = in.nextInt();
        placeDao.allFromPlaces();
        System.out.println("Enter place id: ");
        int place_id = in.nextInt();
        ticketDao.createTicketQuery(id,session_id, place_id, price);
    }

    public void getAll(){
        concertDao.allFromConcerts();
        movieDao.allFromMovies();
        musicalDao.allFromMusicals();
        placeDao.allFromPlaces();
        sessionDao.allFromSessions();
        ticketDao.allFromTickets();
    }
}
