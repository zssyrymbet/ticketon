package kz.iitu.Ticketon;

import kz.iitu.Ticketon.service.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class RunApp {
    Scanner in = new Scanner(System.in);

    @Autowired
    private Test test;

    public void runApp(){
        int choice = 0;
        while(choice != 7){
            System.out.println(" Choose the option by using number! ");
            System.out.println(" 1.Create a concert");
            System.out.println(" 2.Create a movie" );
            System.out.println(" 3.Create a musical");
            System.out.println(" 4.Create a place");
            System.out.println(" 5.Create a session");
            System.out.println(" 6.Create a ticket");
            System.out.println(" 7.Exit");
            choice = in.nextInt();

            switch (choice) {

                case 1:
                    test.createConcert();
                    break;
                case 2:
                    test.createMovie();
                    break;
                case 3:
                    test.createMusical();
                    break;
                case 4:
                    test.createPlace();
                    break;
                case 5:
                    test.createSession();
                    break;
                case 6:
                    test.createTicket();
                    break;
                case 7:
                    test.getAll();
                    System.out.println(" Exit ");
                    System.exit(0);
            }
        }
    }
}