package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.ConcertMapper;
import kz.iitu.Ticketon.dao.mapper.SessionMapper;
import kz.iitu.Ticketon.entity.Concert;
import kz.iitu.Ticketon.entity.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class ConcertDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Concert concert;

    public void createConcertQuery(int id, String name, String description, String location){
        String query = "INSERT INTO concerts (id, name, description, location) VALUES(?, ?, ?, ?)";
        int result = jdbcTemplate.update(query, id, name, description, location);
        if(result > 0){
            System.out.println("You added a new concert!");
        }
    }

    public void allFromConcerts(){
        String concertQuery = "SELECT * FROM concerts";
        List<Concert> concertList = jdbcTemplate.query(concertQuery, new ConcertMapper());
        for (Concert concert: concertList){
            System.out.println(concert);
        }
    }

    public Concert getConcertById(int concert_id){
        String concertQuery = "SELECT * FROM concerts WHERE id = ?";
        List<Concert> concertList = jdbcTemplate.query(concertQuery,new Object[]{concert_id}, new ConcertMapper());
        for (Concert c: concertList){
            concert.setId(c.getId());
            concert.setName(c.getName());
            concert.setDescription(c.getDescription());
            concert.setLocation(c.getLocation());
        }
        return concert;
    }

    public Concert getConcertId(int concert_id){
        String concertQuery = "SELECT * FROM concerts WHERE id = ?";
        concert = jdbcTemplate.queryForObject(concertQuery,new Object[]{concert_id}, new ConcertMapper());
        if(concert != null){
            System.out.println(concert);
        }
        return concert;
    }
}
