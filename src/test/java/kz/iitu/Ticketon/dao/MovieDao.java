package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.MovieMapper;
import kz.iitu.Ticketon.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class MovieDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Movie movie;

    public void createMovieQuery(int id, String name, int year, String country, String description){
        String query = "INSERT INTO movies (id, name, year, country, description) VALUES(?, ?, ?, ?, ?)";
        int result = jdbcTemplate.update(query, id, name, year, country, description);
        if(result > 0){
            System.out.println("You added a new movie!");
        }
    }

    public void allFromMovies(){
        String movieQuery = "SELECT * FROM movies";
        List<Movie> movieList = jdbcTemplate.query(movieQuery, new MovieMapper());
        for (Movie movie: movieList){
            System.out.println(movie);
        }
    }

    public Movie getMovieById(int movie_id){
        String movieQuery = "SELECT * FROM movies WHERE id = ?";
        List<Movie> movieList = jdbcTemplate.query(movieQuery,new Object[]{movie_id}, new MovieMapper());
        for (Movie m: movieList){
            movie.setId(m.getId());
            movie.setName(m.getName());
            movie.setDescription(m.getDescription());
            movie.setYear(m.getYear());
            movie.setCounty(m.getCounty());
        }
        return movie;
    }

    public Movie getMovieId(int movie_id){
        String movieQuery = "SELECT * FROM movies WHERE id = ?";
        movie = jdbcTemplate.queryForObject(movieQuery,new Object[]{movie_id}, new MovieMapper());
        if(movie != null){
            System.out.println(movie);
        }
        return movie;
    }
}
