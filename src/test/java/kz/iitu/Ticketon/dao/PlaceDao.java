package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.PlaceMapper;
import kz.iitu.Ticketon.entity.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class PlaceDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Place place;

    public void createPlaceQuery(int id, String name, String location, int seat){
        String query = "INSERT INTO places (id, name, location, seat) VALUES(?, ?, ?, ?)";
        int result = jdbcTemplate.update(query, id, name, location, seat);
        if(result > 0){
            System.out.println("You added a new place!");
        }
    }

    public void allFromPlaces(){
        String placeQuery = "SELECT * FROM places";
        List<Place> placeList = jdbcTemplate.query(placeQuery, new PlaceMapper());
        for (Place place: placeList){
            System.out.println(place);
        }
    }

    public Place getPlaceById(int place_id){
        String placeQuery = "SELECT * FROM places WHERE id = ?";
        List<Place> placeList = jdbcTemplate.query(placeQuery,new Object[]{place_id}, new PlaceMapper());
        for (Place p: placeList){
            place.setId(p.getId());
            place.setName(p.getName());
            place.setLocation(p.getLocation());
            place.setSeat(p.getSeat());
        }
        return place;
    }

    public Place getPlaceID(int place_id){
        String placeQuery = "SELECT * FROM places WHERE id = ?";
        place = jdbcTemplate.queryForObject(placeQuery,new Object[]{place_id}, new PlaceMapper());
        if(place != null){
            System.out.println(place);
        }
        return place;
    }
}
