package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.TicketMapper;
import kz.iitu.Ticketon.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class TicketDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void createTicketQuery(int id, int session_id, int place_id, double price){
        String query = "INSERT INTO tickets (id, session_id, place_id, price) VALUES(?, ?, ?, ?)";
        int result = jdbcTemplate.update(query, id, session_id, place_id, price);
        if(result > 0){
            System.out.println("You added a new ticket!");
        }
    }

    public void allFromTickets(){
        String ticketQuery = "SELECT * FROM tickets";
        List<Ticket> tickets = jdbcTemplate.query(ticketQuery, new TicketMapper());
        for (Ticket ticket: tickets){
            System.out.println(ticket);
        }
    }
}
