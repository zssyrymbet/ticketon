package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.MusicalMapper;
import kz.iitu.Ticketon.entity.Musical;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class MusicalDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Musical musical;

    public void createMusicalQuery(int id, String name, String description){
        String query = "INSERT INTO musicals (id, name, description) VALUES(?, ?, ?)";
        int result = jdbcTemplate.update(query, id, name, description);
        if(result > 0){
            System.out.println("You added a new musical!");
        }
    }

    public void allFromMusicals(){
        String musicalQuery = "SELECT * FROM musicals";
        List<Musical> musicalList = jdbcTemplate.query(musicalQuery, new MusicalMapper());
        for (Musical musical: musicalList){
            System.out.println(musical);
        }
    }

    public Musical getMusicalById(int musical_id){
        String musicalQuery = "SELECT * FROM musicals WHERE id = ?";
        List<Musical> musicalList = jdbcTemplate.query(musicalQuery,new Object[]{musical_id}, new MusicalMapper());
        for (Musical m: musicalList){
            musical.setId(m.getId());
            musical.setName(m.getName());
            musical.setDescription(m.getDescription());
        }
        return musical;
    }

    public Musical getMusicalID(int musical_id){
        String musicalQuery = "SELECT * FROM musicals WHERE id = ?";
        musical = jdbcTemplate.queryForObject(musicalQuery,new Object[]{musical_id}, new MusicalMapper());
        if(musical != null){
            System.out.println(musical);
        }
        return musical;
    }
}
