package kz.iitu.Ticketon.dao;
import kz.iitu.Ticketon.dao.mapper.SessionMapper;
import kz.iitu.Ticketon.entity.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Scanner;

@Component
public class SessionDao {
    Scanner in = new Scanner(System.in);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Session session;

    public void createSessionQuery(int id, String time, int concert_id, int movie_id, int musical_id){
        String query = "INSERT INTO sessions (id, time, concert_id, movie_id, musical_id) VALUES(?, ?, ?, ?, ?)";
        int result = jdbcTemplate.update(query, id, time, concert_id, movie_id, musical_id);
        if(result > 0){
            System.out.println("You added a new session!");
        }
    }

    public void allFromSessions(){
        String sessionQuery = "SELECT * FROM sessions";
        List<Session> sessions = jdbcTemplate.query(sessionQuery, new SessionMapper());
        for (Session session: sessions){
            System.out.println(session);
        }
    }

    public Session getSessionById(int session_id){
        String sessionQuery = "SELECT * FROM sessions WHERE id = ?";
        session = jdbcTemplate.queryForObject(sessionQuery,new Object[]{session_id}, new SessionMapper());
        if(session != null){
            System.out.println(session);
        }
        return session;
    }
}
