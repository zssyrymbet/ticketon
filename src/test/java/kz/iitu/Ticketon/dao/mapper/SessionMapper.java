package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.dao.*;
import kz.iitu.Ticketon.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SessionMapper implements RowMapper<Session> {
    @Nullable
    @Override
    public Session mapRow(ResultSet rs, int rowNum) throws SQLException {

        Session session = new Session();
        session.setId(rs.getInt("id"));
        int concert_id = rs.getInt("concert_id");
        int movie_id = rs.getInt("movie_id");
        int musical_id = rs.getInt("musical_id");
        if (concert_id == 0) {
            session.setConcert(null);
        }else{
            Concert concert = new ConcertDao().getConcertId(concert_id);
            session.setConcert(concert);
        }

        if (movie_id == 0) {
            session.setMovie(null);
        }else{
            Movie movie = new MovieDao().getMovieId(movie_id);
            session.setMovie(movie);
        }

        if (musical_id == 0) {
            session.setMusical(null);
        }else{
            Musical musical = new MusicalDao().getMusicalById(musical_id);
            session.setMusical(musical);
        }
        session.setTime(rs.getString("time"));

        return session;
    }
}
