package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.entity.Concert;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ConcertMapper implements RowMapper<Concert> {
    @Nullable
    @Override
    public Concert mapRow(ResultSet rs, int rowNum) throws SQLException {

        Concert concert = new Concert();
        concert.setId(rs.getInt("id"));
        concert.setName(rs.getString("name"));
        concert.setDescription(rs.getString("description"));
        concert.setLocation(rs.getString("location"));
        return concert;
    }
}
