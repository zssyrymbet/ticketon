package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.entity.Place;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PlaceMapper implements RowMapper<Place> {
    @Nullable
    @Override
    public Place mapRow(ResultSet rs, int rowNum) throws SQLException {

        Place place = new Place();
        place.setId(rs.getInt("id"));
        place.setName(rs.getString("name"));
        place.setLocation(rs.getString("location"));
        place.setSeat(rs.getInt("seat"));
        return place;
    }
}
