package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.entity.Musical;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MusicalMapper implements RowMapper<Musical> {
    @Nullable
    @Override
    public Musical mapRow(ResultSet rs, int rowNum) throws SQLException {

        Musical musical = new Musical();
        musical.setId(rs.getInt("id"));
        musical.setName(rs.getString("name"));
        musical.setDescription(rs.getString("description"));
        return musical;
    }
}
