package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.entity.Movie;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MovieMapper implements RowMapper<Movie> {
    @Nullable
    @Override
    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {

        Movie movie = new Movie();
        movie.setId(rs.getInt("id"));
        movie.setName(rs.getString("name"));
        movie.setDescription(rs.getString("description"));
        movie.setCounty(rs.getString("country"));
        movie.setYear(rs.getInt("year"));
        return movie;
    }
}