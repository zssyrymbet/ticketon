package kz.iitu.Ticketon.dao.mapper;
import kz.iitu.Ticketon.dao.PlaceDao;
import kz.iitu.Ticketon.dao.SessionDao;
import kz.iitu.Ticketon.entity.Place;
import kz.iitu.Ticketon.entity.Session;
import kz.iitu.Ticketon.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TicketMapper implements RowMapper<Ticket> {

    @Autowired
    private SessionDao sessionDao;

    @Autowired
    private PlaceDao placeDao;

    @Nullable
    @Override
    public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {

        Ticket ticket = new Ticket();
        ticket.setId(rs.getInt("id"));
        int session_id = rs.getInt("session_id");
        int place_id = rs.getInt("place_id");
        Session session = sessionDao.getSessionById(session_id);
        if (session != null) {
            ticket.setSession(session);
        }
        Place place = placeDao.getPlaceById(place_id);
        if (place != null) {
            ticket.setPlace(place);
        }

        ticket.setPrice(rs.getDouble("price"));

        return ticket;
    }
}
