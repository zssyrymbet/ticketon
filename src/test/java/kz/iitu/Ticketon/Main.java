package kz.iitu.Ticketon;

import kz.iitu.Ticketon.config.SpringConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        RunApp app = context.getBean("runApp", RunApp.class);
        app.runApp();
    }

}
