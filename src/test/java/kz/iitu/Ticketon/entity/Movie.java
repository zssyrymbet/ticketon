package kz.iitu.Ticketon.entity;

import org.springframework.stereotype.Component;

@Component
public class Movie {
    private int id;
    private String name;
    private int year;
    private String county;
    private String description;

    public int getId() {
        return id;
    }

    public Movie() {}

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", county='" + county + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
