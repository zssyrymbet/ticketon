package kz.iitu.Ticketon.entity;

public class Session {
    private int id;
    private String time;
    private Concert concert;
    private Movie movie;
    private Musical musical;

    public Session() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Concert getConcert() {
        return concert;
    }

    public void setConcert(Concert concert) {
        this.concert = concert;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Musical getMusical() {
        return musical;
    }

    public void setMusical(Musical musical) {
        this.musical = musical;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", concert=" + concert +
                ", movie=" + movie +
                ", musical=" + musical +
                '}';
    }
}
